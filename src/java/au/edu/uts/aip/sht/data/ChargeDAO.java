package au.edu.uts.aip.sht.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ChargeDAO {

    public void addCharge(String username, int amount, java.util.Date expiryDate) throws SQLException, NamingException {
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/aip");

        // Try-with-resources to ensure connections are
        // closed automatically.
        try (Connection conn = ds.getConnection()) {

            // Prepare statement and intercept username, hashed password and fullname.
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO \"CHARGE\" (username, amount, expiry) VALUES (?, ?, ?)");
            pstmt.setString(1, username);
            pstmt.setInt(2, amount);
            
            if (expiryDate == null) {
                pstmt.setDate(3, null);
            } else {
                java.sql.Date date = new java.sql.Date(expiryDate.getTime());
                pstmt.setDate(3, date);
            }
            
            // Execute query.
            pstmt.execute();

        }
    }

    public ArrayList<ChargeDTO> findSingle(String token) throws NamingException, SQLException, ParseException, IOException {
        // Initialise variables and declarations.
        ArrayList<UserDTO> userResults = new ArrayList<>();
        ArrayList<ChargeDTO> chargeResults = new ArrayList<>();

        // Initilise datasource.
        DataSource ds = (DataSource) InitialContext.doLookup("jdbc/aip");

        // Try-with-resources to ensure connections are
        // closed automatically.
        try (Connection conn = ds.getConnection()) {

            // Create query, gets user from table based on token
            String query = "SELECT FULLNAME, TOKEN FROM \"USER\" WHERE TOKEN = ?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, token);
            ResultSet rs = stmt.executeQuery();

            // Iterate through returned rows in resultset.
            while (rs.next()) {
                // Temp DTO declared to be added to
                // the arraylist.
                UserDTO user = new UserDTO();
                user.setFullname(rs.getString("FULLNAME"));
                user.setToken(rs.getString("TOKEN"));

                // Add temp DTO to arraylist.
                userResults.add(user);
            }
        }

        for (UserDTO userResult : userResults) {
            String stringUrl = "https://test-api.pin.net.au/1/customers/" + userResult.getToken() + "/charges";

            URL url = new URL(stringUrl);
            URLConnection uc = url.openConnection();

            uc.setRequestProperty("X-Requested-With", "Curl");
            uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");

            InputStreamReader inputReader = new InputStreamReader(uc.getInputStream());
            StringBuilder sb = new StringBuilder();

            try (BufferedReader bufferReader = new BufferedReader(inputReader)) {
                String line;
                while ((line = bufferReader.readLine()) != null) {
                    sb.append(line);
                }
            }

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(sb.toString());
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray arrayJSON = (JSONArray) jsonObject.get("response");

            for (Object chargesJSON : arrayJSON) {
                JSONObject entry = (JSONObject) chargesJSON;
                String amount = ((Long) entry.get("amount")).toString();
                String date = (String) entry.get("created_at");

                ChargeDTO charge = new ChargeDTO();
                charge.setName(userResult.getFullname());
                charge.setAmount(amount);
                charge.setDate(date);

                chargeResults.add(charge);
            }
        }

        return chargeResults;
    }
}
