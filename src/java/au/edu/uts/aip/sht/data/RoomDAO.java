package au.edu.uts.aip.sht.data;

import java.sql.*;
import java.util.*;
import javax.naming.*;
import javax.sql.*;

public class RoomDAO implements RoomDAOInterface {
	
	// Find a room based on the integer based ID.
	@Override
	public RoomDTO getRoomByID(int id) throws NamingException, SQLException {
		
		// Initialise variables and declarations.
		RoomDTO bDTO = new RoomDTO();
		
		
		// SQL statement. Cannot be turned into a view in Derby/JDBC
		// due to requiring parameters.
		String sql = "SELECT ID, b.USERNAME, b.FULLNAME\n" +
			"FROM AIP.\"ROOM\" a\n" +
			"LEFT OUTER JOIN AIP.\"USER\" b\n" +
			"ON a.USERNAME = b.USERNAME WHERE ID = ?";

		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept passed ID.
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			
			// Execute query and return result set.
			ResultSet rs = ps.executeQuery();
			
			
			// "if next" is used over "while next" as this
			// is only for one row.
			if (rs.next()) {
				
				// Add retrieved data to the DTO.
				bDTO.setId(rs.getInt("ID"));
				bDTO.setUsername(rs.getString("username"));
				bDTO.setFullname(rs.getString("fullname"));

			}
			
		}
		
		// Return the DTO.
		return bDTO;
	}

	
	// Return all rooms in the database.
	@Override
	public ArrayList<RoomDTO> findAll() throws NamingException, SQLException {

		// Initialise variables and declarations.
		ArrayList<RoomDTO> bDTO = new ArrayList<RoomDTO>();
		
		
		// Retrieve all rows from pre-defined view.
		// Asterisk avoided due to bad practice.
		String sql = "SELECT ID, USERNAME, FULLNAME, RESERVED FROM FINDALLROOMS";
		
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept passed ID.
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			
			// Iterate through returned rows in resultset.
			while (rs.next()) {
				
				// Temp DTO declared to be added to
				// the arraylist.
				RoomDTO temp = new RoomDTO();
				temp.setId(rs.getInt("ID"));
				temp.setUsername(rs.getString("username"));
				temp.setFullname(rs.getString("fullname"));
				temp.setReserved(rs.getBoolean("reserved"));
				
				// Add temp DTO to arraylist.
				bDTO.add(temp);
			}

		}
		
		// Return the DTO arraylist.
		return bDTO;
		
	}
	
	
	
	@Override
	public void makeBooking(int id, String username) throws NamingException, SQLException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept passed ID.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE ROOM SET USERNAME = ? WHERE ID = ?");
			
			
			// Prepare statement and intercept parameters.
			pstmt.setString(1, username);
			pstmt.setInt(2, id);
		
			
			// Execute query.
			pstmt.execute();
			
		}
			
	}
	
	
	@Override
	public void cancelBooking(int id) throws NamingException, SQLException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept passed ID.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE ROOM SET USERNAME = NULL WHERE ID = ?");
			
			
			// Prepare statement and intercept parameters.
			pstmt.setInt(1, id);
		
			
			// Execute query.
			pstmt.execute();
			
		}
			
	}
	
	
	
	@Override
	public void makeReservation(int id, String username) throws NamingException, SQLException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept passed ID.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE ROOM SET USERNAME = ?, RESERVED = TRUE WHERE ID = ?");
			
			
			// Prepare statement and intercept parameters.
			pstmt.setString(1, username);
			pstmt.setInt(2, id);
		
			
			// Execute query.
			pstmt.execute();
			
		}
			
	}
	
	
	@Override
	public void cancelReservation(int id) throws NamingException, SQLException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept passed ID.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE ROOM SET USERNAME = NULL, RESERVED = FALSE WHERE ID = ?");
			
			
			// Prepare statement and intercept parameters.
			pstmt.setInt(1, id);
		
			
			// Execute query.
			pstmt.execute();
			
		}
			
	}
	
	
}
