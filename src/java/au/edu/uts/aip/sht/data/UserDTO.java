package au.edu.uts.aip.sht.data;

import java.util.Date;

public class UserDTO {
	
	private String username;
        private String password;
        private String fullname;
	private int subscription;
        private String token;
        private Date subexpire;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public int getSubscription() {
		return subscription;
	}

	public void setSubscription(int subscription) {
		this.subscription = subscription;
	}
        
        public String getToken(){
            return token;
        }
        
        public void setToken(String token){
            this.token = token;
        }
        
        public void setSubexpire(Date subexpire){
            this.subexpire = subexpire;
        }
        
        public Date getSubexpire(){
            return this.subexpire;
        }
	
}
