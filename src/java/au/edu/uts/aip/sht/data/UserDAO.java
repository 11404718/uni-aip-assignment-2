package au.edu.uts.aip.sht.data;

import static au.uts.edu.aip.sht.domain.Hash.hash256;
import java.security.*;
import java.sql.*;
import javax.sql.*;
import java.util.*;
import javax.naming.*;

public class UserDAO implements UserDAOInterface {
	
	// Find user from database based on the user's name.
	public UserDTO find(String username) throws NamingException, SQLException {

		// Initialise variables and declarations.
		UserDTO result = new UserDTO();
		
		
		// Initialise the datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username.
			PreparedStatement stmt = conn.prepareStatement("SELECT USERNAME, PASSWORD, FULLNAME, SUBSCRIPTION, TOKEN, SUBEXPIRYDATE FROM \"USER\" WHERE USERNAME = ?");
			stmt.setString(1, username);

			
			// Execute query and return result set.
			ResultSet rs = stmt.executeQuery();


			// "if next" is used over "while next" as this
			// is only for one row.
			if (rs.next()) {
				
				// Add retrieved data to the DTO.
				result.setUsername(rs.getString("USERNAME"));
				result.setPassword(rs.getString("PASSWORD"));
				result.setFullname(rs.getString("FULLNAME"));
				result.setSubscription(rs.getInt("SUBSCRIPTION"));
                                result.setToken(rs.getString("TOKEN"));
                                result.setSubexpire(rs.getDate("SUBEXPIRYDATE"));
				
			}
			
		}
		
		// Return the DTO.
		return result;
	
	}
	
	
	// Return all users in the database.
	public ArrayList<UserDTO> findAll() throws NamingException, SQLException {
		
		// Initialise variables and declarations.
		ArrayList<UserDTO> result = new ArrayList<>();
		
		
		// Initilise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Create statement. No PreparedStatement is required due
			// to lack of parameters.
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT USERNAME, PASSWORD, FULLNAME, SUBSCRIPTION, TOKEN, SUBEXPIRYDATE FROM \"USER\"");

			
			// Iterate through returned rows in resultset.
			while (rs.next()) {
				
				// Temp DTO declared to be added to
				// the arraylist.
				UserDTO user = new UserDTO();
				user.setUsername(rs.getString("USERNAME"));
				user.setPassword(rs.getString("PASSWORD"));
				user.setFullname(rs.getString("FULLNAME"));
				user.setSubscription(rs.getInt("SUBSCRIPTION"));
                                user.setToken(rs.getString("TOKEN"));
                                user.setSubexpire(rs.getDate("SUBEXPIRYDATE"));
				
				// Add temp DTO to arraylist.
				result.add(user);
			}
		}
		
		// Return the DTO arraylist.
		return result;
		
	}
	
	
	// Allow a new user to register for the system.
	public void register(String username, String password, String fullname, int subscription) throws NamingException, SQLException, NoSuchAlgorithmException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username, hashed password and fullname.
			PreparedStatement pstmt = conn.prepareStatement("INSERT INTO \"USER\" VALUES (?, ?, ?, FALSE, ?, NULL, NULL)");
			pstmt.setString(1, username);
			pstmt.setString(2, hash256(password));
			pstmt.setString(3, fullname);
			pstmt.setInt(4, subscription);

			
			// Execute query.
			pstmt.execute();
		
		}
		
	}
	
	
	// Allow a user to change their password.
	public void changePassword(String username, String password) throws NamingException, SQLException, NoSuchAlgorithmException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username, hashed password and fullname.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE \"USER\" SET PASSWORD = ? WHERE USERNAME = ?");
			pstmt.setString(1, hash256(password));
			pstmt.setString(2, username);

			// Execute query.
			pstmt.execute();
			
		}
		
	}
        
        /**
         * update SubExpire
         * @param username
         * @param date
         * @throws SQLException
         * @throws NamingException 
         */
        public void updateSubExpire(String username, java.util.Date expiryDate) throws SQLException, NamingException{
            // Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username, hashed password and fullname.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE \"USER\" SET SUBEXPIRYDATE = ? WHERE USERNAME = ?");
			java.sql.Date date = new java.sql.Date(expiryDate.getTime());
                        
                        pstmt.setDate(1, date);
			pstmt.setString(2, username);

			// Execute query.
			pstmt.execute();
			
		}
        }
        
        // Update the token
        public void changeToken(String username, String token) throws NamingException, SQLException, NoSuchAlgorithmException{
            // Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username, hashed password and fullname.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE \"USER\" SET TOKEN = ? WHERE USERNAME = ?");
			pstmt.setString(1, token);
			pstmt.setString(2, username);

			// Execute query.
			pstmt.execute();
			
		}
        }
	
	
	// Allow a user to delete their own account.
	public String deleteAccount(String username) throws NamingException, SQLException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
		
			// Prepare statement and intercept username.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE \"USER\" SET DELETED = TRUE WHERE USERNAME = ?");
			pstmt.setString(1, username);

			// Execute query.
			pstmt.execute();
			
		}
		
		// Return to account maintenance page.
		return "account";
		
	}
	
	
	// Allow a user to change their subscription.
	public void modifySubscription(String username, int subscription) throws NamingException, SQLException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username + new subscription.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE \"USER\" SET SUBSCRIPTION = ? WHERE USERNAME = ?");
			pstmt.setInt(1, subscription);
			pstmt.setString(2, username);

			// Execute query.
			pstmt.execute();
			
		}
		
	}
	
	
}