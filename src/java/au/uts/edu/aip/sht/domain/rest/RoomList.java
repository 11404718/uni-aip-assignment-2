package au.uts.edu.aip.sht.domain.rest;

import au.edu.uts.aip.sht.data.RoomDTO;
import au.uts.edu.aip.sht.domain.controller.RoomListController;
import java.sql.SQLException;
import java.util.*;
import javax.annotation.*;
import javax.ejb.*;
import javax.naming.NamingException;
import javax.ws.rs.*;

@Path("roomlist")
public class RoomList {

    private LinkedList<RoomDTO> rooms = new LinkedList<RoomDTO>();

    @PostConstruct
    protected void init() {
        try {
            RoomListController tempController = new RoomListController();
            rooms.addAll(tempController.getRooms());
        } catch (NamingException | SQLException e) {
            rooms = null;
        }
    }

    @GET
    public LinkedList<RoomDTO> peek() {
        return rooms;
    }

}