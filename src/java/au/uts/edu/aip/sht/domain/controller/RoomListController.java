package au.uts.edu.aip.sht.domain.controller;

import au.edu.uts.aip.sht.data.RoomDAO;
import au.edu.uts.aip.sht.data.RoomDTO;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.enterprise.context.*;
import javax.inject.*;
import javax.naming.*;

@Named
@RequestScoped
public class RoomListController implements Serializable {
	
	// Return all rooms from the RoomDAO.
	public Collection<RoomDTO> getRooms() throws NamingException, SQLException {
		RoomDAO dao = new RoomDAO();
		return dao.findAll();
	}
	
}