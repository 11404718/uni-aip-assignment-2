package au.uts.edu.aip.sht.domain.controller;

import au.edu.uts.aip.sht.data.UserDAO;
import au.edu.uts.aip.sht.data.UserDTO;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.enterprise.context.*;
import javax.inject.*;
import javax.naming.*;

@Named
@RequestScoped
public class UserListController implements Serializable {
	
	// Return all users from the UserDAO.
	public Collection<UserDTO> getUsers() throws NamingException, SQLException {
		UserDAO dao = new UserDAO();
		return dao.findAll();
	}
	
}