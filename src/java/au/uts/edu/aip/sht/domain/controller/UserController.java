package au.uts.edu.aip.sht.domain.controller;

import au.edu.uts.aip.sht.data.*;
import au.uts.edu.aip.sht.domain.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.faces.application.*;
import javax.enterprise.context.*;
import javax.faces.context.*;
import javax.inject.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.validation.constraints.*;
import org.json.simple.*;
import org.json.simple.parser.*;

@Named
@SessionScoped
public class UserController implements Serializable {

    private String username;
    private String password;
    private String fullname;
    private int subscription;

    private String address1;
    private String address2;
    private String city;
    private String postcode;
    private String state;
    private String country;

    private String cardnumber;
    private String token;
    private String cvc;
    private String expiremonth;
    private String expireyear;
    private String subExpire;

    // Allow the user to log in via container based security.
    // Reference is Week5 lab.
    public String login() throws NamingException, SQLException {

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        try {
            request.login(username, password);
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage("The supplied credentials were incorrect. Please try again."));
            logout();
            return null;
        } finally {

            // Sets other user values in the UserController,
            // including subscription and full name.
            UserDAO dao = new UserDAO();
            UserDTO loginUserDTO = dao.find(username);
            this.fullname = loginUserDTO.getFullname();
            this.subscription = loginUserDTO.getSubscription();
            this.token = loginUserDTO.getToken();
            this.subExpire = "";

            if (loginUserDTO.getSubexpire() != null) {
                this.subExpire = loginUserDTO.getSubexpire().toString();
            }

        }
        return "/system/home?faces-redirect=true";

    }

    // Allow the user to log out of container based security.
    // Reference is Week5 lab.
    public String logout() {

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.logout();
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }

        return "/login?faces-redirect=true";

    }

    public String register() throws NamingException, SQLException, NoSuchAlgorithmException, IOException {

        // Initialises a new UserDAO to create the new user.
        UserDAO dao = new UserDAO();
        dao.register(username, password, fullname, subscription);

        CreditCard card = new CreditCard(cardnumber, fullname,
                expiremonth, expireyear, cvc, address1, address2, city,
                postcode, state, country);
        chargeCustomer(card);

        // Destroys the UserConroller and returns to the homepage.
        return logout();

    }

    public String changePassword() throws NamingException, SQLException, NoSuchAlgorithmException {

        // Initialises a new UserDAO to create the new user.
        UserDAO dao = new UserDAO();
        dao.changePassword(username, password);

        // Redirect to account management page.
        return "/system/account?faces-redirect=true";

    }

    public String deleteAccount() throws NamingException, SQLException {

        // Initialises a new UserDAO to create the new user.
        UserDAO dao = new UserDAO();
        dao.deleteAccount(username);

        // Destroys the UserController and returns to the homepage.
        return logout();

    }

    // METHOD NEEDS COMMENTING
    public String modifySubscription() throws NamingException, SQLException, IOException {

        // Initialises a new UserDAO to modify the subscription.
        UserDAO dao = new UserDAO();
        dao.modifySubscription(username, subscription);

        CreditCard card = new CreditCard(cardnumber, this.fullname);

        chargeCustomer(card);
        //get user token if there is from the database
        // Redirect to account management page.
        return "/system/modifysubscription?faces-redirect=true";
    }

    private void chargeCustomer(CreditCard card) throws IOException, SQLException, NamingException {
        if (token == null || token.equals("")) {
            System.out.println("New token generation");
            try {
                //get a new customer token
                token = registerToken(username + "@test.com.au", card.getCardNum(),
                        card.getExpiryMonth(), card.getExpiryYear(), card.getCvc(),
                        card.getName(), card.getAddressLineOne(), card.getAddressLineTwo(),
                        card.getCity(), card.getPostcode(), card.getState(), card.getCountry()
                );
                //once we get the token update the database
                UserDAO dao = new UserDAO();
                dao.changeToken(username, token);

            } catch (NamingException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ProtocolException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (subscription > 1) {
            chargeToken(token);
        }
    }

    private void chargeToken(String token) throws IOException, SQLException, NamingException {
        String stringUrl = "https://test-api.pin.net.au/1/charges";
        URL url = new URL(stringUrl);
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        uc.setDoOutput(true);
        uc.setInstanceFollowRedirects(false);
        uc.setRequestMethod("POST");
        uc.setRequestProperty("X-Requested-With", "Curl");
        uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");

        int amount = getAmount();

        DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
        wr.writeBytes("email=" + username + "@test.com.au" + "&"
                + "description=Subscription charge&"
                + "amount=" + amount + "&" + "ip_address=203.192.1.172&"
                + "customer_token=" + token); //customer token here
        wr.flush();
        wr.close();

        InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());

        System.out.println(uc.getResponseCode());
        BufferedReader reader = new BufferedReader(reader2);
        StringBuilder out2
                = new StringBuilder();
        String line;
        while ((line = reader.readLine())
                != null) {
            out2.append(line);
        }

        System.out.println(out2.toString());

        if (uc.getResponseCode() == 201) {
            showError("Subscription value charged.");
            topUpSubscription(amount);
        }
        reader.close();
    }

    private int getAmount() {
        if (subscription == 2) {
            return 1100;
        } else if (subscription == 3) {
            return 2000;
        }
        return 0;
    }

    private void showError(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    private void topUpSubscription(int amount) throws SQLException, NamingException {

        UserDAO dao = new UserDAO();
        UserDTO loginUserDTO = dao.find(username);

        Calendar calendar = Calendar.getInstance();

        if (loginUserDTO.getSubexpire() == null) {
            calendar.add(Calendar.DATE, 30);
        } else {
            calendar.setTime(loginUserDTO.getSubexpire());
            calendar.add(Calendar.DATE, 30);
        }

        ChargeDAO chargeDAO = new ChargeDAO();
        UserDAO userDAO = new UserDAO();

        chargeDAO.addCharge(username, amount, calendar.getTime());
        userDAO.updateSubExpire(username, calendar.getTime());

    }

    // NEEDS COMMENTING
    public String registerToken(String email, String cardNum, String cardMonthExp,
            String cardYearExp, String cardCvc, String cardName,
            String cardAddress1, String cardAddress2, String cardCity,
            String cardPostcode, String cardState, String cardCountry)
            throws NamingException, SQLException, NoSuchAlgorithmException,
            MalformedURLException, ProtocolException, IOException, ParseException {

        String stringUrl = "https://test-api.pin.net.au/1/customers";
        String toReturn = "";
        URL url = new URL(stringUrl);
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();

        uc.setDoOutput(true);
        uc.setInstanceFollowRedirects(false);
        uc.setRequestMethod("POST");
        uc.setRequestProperty("X-Requested-With", "Curl");
        uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");

        DataOutputStream wr = new DataOutputStream(uc.getOutputStream());

        wr.writeBytes("email=" + email + "&"
                + "card[number]=" + cardNum + "&"
                + "card[expiry_month]=" + cardMonthExp + "&"
                + "card[expiry_year]=" + cardYearExp + "&"
                + "card[cvc]=" + cardCvc + "&"
                + "card[name]=" + cardName + "&"
                + "card[address_line1]=" + cardAddress1 + "&"
                + "card[address_line2]=" + cardAddress2 + "&"
                + "card[address_city]=" + cardCity + "&"
                + "card[address_postcode]=" + cardPostcode + "&"
                + "card[address_state]=" + cardState + "&"
                + "card[address_country]=" + cardCountry); // card details here OR we can use card tokens...?

        wr.flush();
        wr.close();

        InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());

        BufferedReader reader = new BufferedReader(reader2);
        StringBuilder out2 = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out2.append(line);
        }

        System.out.println(out2.toString());
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(out2.toString());

        JSONObject jsonObject = (JSONObject) obj;

        JSONObject responseJSON = (JSONObject) jsonObject.get("response");
        toReturn = (String) responseJSON.get("token");

        return toReturn;
    }

    // METHOD NEEDS COMMENTING
    public boolean getIsAdmin() {
        return subscription == 4;
    }

    // METHOD NEEDS COMMENTING
    public String getSubscriptiontext() throws NamingException, SQLException {

        UserDAO dao = new UserDAO();
        UserDTO loginUserDTO = dao.find(username);
        
        if (loginUserDTO.getSubexpire() != null) {
            this.subExpire = loginUserDTO.getSubexpire().toString();
        }
        switch (subscription) {
            case 0:
                return "No subscription type defined.";
            case 1:
                return "Free User";
            case 2:
                return "Standard User";
            case 3:
                return "Premium User";
            case 4:
                return "Administrator";
            default:
                return "No subscription type defined.";
        }
    }

    @Size(min = 3)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Size(min = 8)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Digits(integer = 16, fraction = 0)
    @Size(min = 16, max = 16)
    public String getCardNumber() {
        return cardnumber;
    }

    public void setCardNumber(String creditcard) {
        this.cardnumber = creditcard;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getSubscription() {
        return subscription;
    }

    public void setSubscription(int subscription) {
        this.subscription = subscription;
    }

    @Digits(integer = 3, fraction = 0)
    @Size(min = 3, max = 3)
    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    @Digits(integer = 2, fraction = 0)
    @Size(min = 2, max = 2)
    public String getExpiremonth() {
        return expiremonth;
    }

    public void setExpiremonth(String expiremonth) {
        this.expiremonth = expiremonth;
    }

    @Digits(integer = 4, fraction = 0)
    @Size(min = 4, max = 4)
    public String getExpireyear() {
        return expireyear;
    }

    public void setExpireyear(String expireyear) {
        this.expireyear = expireyear;
    }

    public String getSubExpire() {
        return subExpire;
    }

    public void setSubExpire(String subExpire) {
        this.subExpire = subExpire;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Digits(integer = 4, fraction = 0)
    @Size(min = 4, max = 4)
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
