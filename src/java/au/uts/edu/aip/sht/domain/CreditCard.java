package au.uts.edu.aip.sht.domain;

public class CreditCard {

	private String cardNum;
	private String name;
	private String expiryMonth;
	private String expiryYear;
	private String cvc;
	private String addressLineOne;
	private String addressLineTwo;
	private String city;
	private String postcode;
	private String state;
	private String country;

	public CreditCard(String number, String name,
			String expiryMonth, String expiryYear, String cvc,
			String addressLineOne, String addressLineTwo, String city,
			String postcode, String state, String country) {
		this.cardNum = number;
		this.name = name;
		this.expiryMonth = expiryMonth;
		this.expiryYear = expiryYear;
		this.cvc = cvc;
		this.addressLineOne = addressLineOne;
		this.addressLineTwo = addressLineTwo;
		this.city = city;
		this.postcode = postcode;
		this.state = state;
		this.country = country;
	}

	public CreditCard(String number, String name) {
		this.cardNum = number;
		this.name = name;

		this.expiryMonth = "03";
		this.expiryYear = "2017";
		this.cvc = "123";

		this.addressLineOne = "123 Something St";
		this.addressLineTwo = "";
		this.city = "Sydney";
		this.postcode = "2000";
		this.state = "NSW";
		this.country = "Australia";
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCardNum() {
		return cardNum;
	}

	public String getExpiryMonth() {
		return expiryMonth;
	}

	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	public String getExpiryYear() {
		return expiryYear;
	}

	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	public String getCvc() {
		return cvc;
	}

	public void setCvc(String cvc) {
		this.cvc = cvc;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}