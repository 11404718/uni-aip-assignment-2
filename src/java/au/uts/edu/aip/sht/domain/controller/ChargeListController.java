package au.uts.edu.aip.sht.domain.controller;

import au.edu.uts.aip.sht.data.ChargeDAO;
import au.edu.uts.aip.sht.data.ChargeDTO;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.naming.NamingException;
import org.json.simple.parser.ParseException;

@Named
@RequestScoped
public class ChargeListController implements Serializable {

    // Returns charges of a single user from ChargeDAO
    public Collection<ChargeDTO> getCharge(String token) throws NamingException, SQLException, ParseException, IOException {
        ChargeDAO dao = new ChargeDAO();
        return dao.findSingle(token);
    }
}
