package au.uts.edu.aip.sht.domain.controller;
import au.edu.uts.aip.sht.data.ChargeDAO;
import au.edu.uts.aip.sht.data.UserDAO;
import au.edu.uts.aip.sht.data.UserDTO;
import au.edu.uts.aip.sht.data.RoomDAO;
import au.edu.uts.aip.sht.data.RoomDTO;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.*;
import javax.naming.*;

@Named
@RequestScoped
public class RoomController implements Serializable {

    private int id;
    private String username;

    // Returns RoomDTO (row) based on Room's ID (primary key).
    public RoomDTO getRoomByID(int id) throws NamingException, SQLException {
        RoomDAO dao = new RoomDAO();
        return dao.getRoomByID(id);
    }

    // Makes a booking based on GET parameter for ID and logged in user.
    public String makeBooking(String username) throws NamingException, SQLException {

        // Initialises a new UserDAO to create the new user.
        RoomDAO dao = new RoomDAO();

		// Sources row parameter as opposed to input field for Room ID due to buttons
        // for each row.
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        int passedInt = Integer.parseInt(ec.getRequestParameterMap().get("id"));

        // Make booking.
        dao.makeBooking(passedInt, username);

        // Refresh page.
        return "booking?faces-redirect=true";

    }

    // Cancels a booking based on GET parameter for ID and logged in user.
    public String cancelBooking() throws NamingException, SQLException {

        // Initialises a new UserDAO to create the new user.
        RoomDAO dao = new RoomDAO();
        UserDAO userdao = new UserDAO();

		// Sources row parameter as opposed to input field for Room ID due to buttons
        // for each row.
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        int passedInt = Integer.parseInt(ec.getRequestParameterMap().get("id"));

        RoomDTO room = dao.getRoomByID(passedInt);
        UserDTO user = userdao.find(room.getUsername());
        if(user.getSubscription() == 1)
            chargeCancellationFee(user);

		// Make booking.
        dao.cancelBooking(passedInt);
        // Refresh page.
        return "booking?faces-redirect=true";
    }

    private void chargeCancellationFee(UserDTO user) {
        try {
            String stringUrl = "https://test-api.pin.net.au/1/charges";
            URL url = new URL(stringUrl);
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.setDoOutput(true);
            uc.setInstanceFollowRedirects(false);
            uc.setRequestMethod("POST");
            uc.setRequestProperty("X-Requested-With", "Curl");
            uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
            
            int amount = 3000;
            
            DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
            wr.writeBytes("email=" + user.getUsername() + "@test.com.au" + "&"
                    + "description=Subscription charge&"
                    + "amount=" + amount + "&" + "ip_address=203.192.1.172&"
                    + "customer_token=" + user.getToken()); //customer token here
            wr.flush();
            wr.close();
            
            InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
            
            BufferedReader reader = new BufferedReader(reader2);
            StringBuilder out2
                    = new StringBuilder();
            String line;
            while ((line = reader.readLine())
                    != null) {
                out2.append(line);
            }
            
            System.out.println(out2.toString());
            
            if (uc.getResponseCode() == 201) {
                System.out.println("Successfully charged cancellation fee.");
                ChargeDAO chargeDAO = new ChargeDAO();
                chargeDAO.addCharge(user.getUsername(), amount, null);
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(RoomController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ProtocolException ex) {
            Logger.getLogger(RoomController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RoomController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(RoomController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NamingException ex) {
            Logger.getLogger(RoomController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    

    // Makes a reservation. based on GET parameter for ID and logged in user.
    public String makeReservation(String username) throws NamingException, SQLException {

        // Initialises a new UserDAO to create the new user.
        RoomDAO dao = new RoomDAO();

		// Sources row parameter as opposed to input field for Room ID due to buttons
        // for each row.
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        int passedInt = Integer.parseInt(ec.getRequestParameterMap().get("id"));

        // Make booking.
        dao.makeReservation(passedInt, username);

        // Refresh page.
        return "booking?faces-redirect=true";

    }

    // Cancels a booking based on GET parameter for ID and logged in user.
    public String cancelReservation() throws NamingException, SQLException {

        // Initialises a new UserDAO to create the new user.
        RoomDAO dao = new RoomDAO();

		// Sources row parameter as opposed to input field for Room ID due to buttons
        // for each row.
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        int passedInt = Integer.parseInt(ec.getRequestParameterMap().get("id"));

        // Make booking.
        dao.cancelReservation(passedInt);

        // Refresh page.
        return "booking?faces-redirect=true";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
