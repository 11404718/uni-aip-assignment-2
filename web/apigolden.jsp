<%@page import="org.json.simple.parser.ContainerFactory"%>
<%@page import="org.json.simple.JSONValue"%>
<%@page import="java.io.IOException"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    // GET /charges
    /**
     * String stringUrl = "https://test-api.pin.net.au/1/charges"; URL url = new
     * URL(stringUrl); URLConnection uc = url.openConnection();
     *
     * uc.setRequestProperty("X-Requested-With", "Curl");
     * uc.setRequestProperty("Authorization", "Basic
     * YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
     *
     * InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
     *
     * BufferedReader reader = new BufferedReader(reader2); StringBuilder out2 =
     * new StringBuilder(); String line; while ((line = reader.readLine()) !=
     * null) { out2.append(line); } System.out.println(out2.toString());
     * reader.close();
     *
     */

    // GET /charges
    /**
     * String stringUrl = "https://test-api.pin.net.au/1/charges"; URL url = new
     * URL(stringUrl); URLConnection uc = url.openConnection();
     *
     * uc.setRequestProperty("X-Requested-With", "Curl");
     * uc.setRequestProperty("Authorization", "Basic
     * YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
     *
     * InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
     *
     * BufferedReader reader = new BufferedReader(reader2); StringBuilder out2 =
     * new StringBuilder(); String line; while ((line = reader.readLine()) !=
     * null) { out2.append(line); } System.out.println(out2.toString());
     * reader.close();
     *
     */
    // POST /charges
    /**
     * String stringUrl = "https://test-api.pin.net.au/1/charges"; URL url = new
     * URL(stringUrl); HttpURLConnection uc =
     * (HttpURLConnection)url.openConnection();
     *
     * uc.setDoOutput(true); uc.setInstanceFollowRedirects(false);
     * uc.setRequestMethod("POST"); uc.setRequestProperty("X-Requested-With",
     * "Curl"); uc.setRequestProperty("Authorization", "Basic
     * YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
     *
     * DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
     * wr.writeBytes("email=test@test.com.au&" + "description=something&" +
     * "amount=400&" + "ip_address=203.192.1.172&" +
     * "customer_token=cus_l1BAUZpQwsNiiSHRLFJrYA"); //customer token here
     * wr.flush(); wr.close();
     *
     * InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
     *
     * BufferedReader reader = new BufferedReader(reader2); StringBuilder out2 =
     * new StringBuilder(); String line; while ((line = reader.readLine()) !=
     * null) { out2.append(line); } System.out.println(out2.toString());
     * reader.close();
     *
     */
    // POST /charges
    /**
     * String stringUrl = "https://test-api.pin.net.au/1/customers"; URL url =
     * new URL(stringUrl); HttpURLConnection uc =
     * (HttpURLConnection)url.openConnection();
     *
     * uc.setDoOutput(true); uc.setInstanceFollowRedirects(false);
     * uc.setRequestMethod("POST"); uc.setRequestProperty("X-Requested-With",
     * "Curl"); uc.setRequestProperty("Authorization", "Basic
     * YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
     *
     * DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
     *
     * wr.writeBytes("email=atestemail@test.com.au&" +
     * "card[number]=5520000000000000&" + "card[expiry_month]=03&" +
     * "card[expiry_year]=2017&" + "card[cvc]=123&" + "card[name]=Simon Test&" +
     * "card[address_line1]=123 Something St&" + "card[address_line2]=&" +
     * "card[address_city]=Sydney&" + "card[address_postcode]=2000&" +
     * "card[address_state]=NSW&" + "card[address_country]=Australia"); // card
     * details here OR we can use card tokens...? wr.flush(); wr.close();
     *
     * InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
     *
     * BufferedReader reader = new BufferedReader(reader2); StringBuilder out2 =
     * new StringBuilder(); String line; while ((line = reader.readLine()) !=
     * null) { out2.append(line); } System.out.println(out2.toString());
     * reader.close();*
     */
    // GET /charges/charge-token
    /**
     * String stringUrl =
     * "https://test-api.pin.net.au/1/charges/ch_DovX9REwH3vku5_849Hw-w"; //
     * using a charge token URL url = new URL(stringUrl); URLConnection uc =
     * url.openConnection();
     *
     * uc.setRequestProperty("X-Requested-With", "Curl");
     * uc.setRequestProperty("Authorization", "Basic
     * YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
     *
     * InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
     *
     * BufferedReader reader = new BufferedReader(reader2); StringBuilder out2 =
     * new StringBuilder(); String line; while ((line = reader.readLine()) !=
     * null) { out2.append(line); } System.out.println(out2.toString());
     * reader.close();
     *
     */
    // GET /customers/customer-token
    /**
     * String stringUrl =
     * "https://test-api.pin.net.au/1/customers/cus_l1BAUZpQwsNiiSHRLFJrYA/";
     * URL url = new URL(stringUrl); URLConnection uc = url.openConnection();
     *
     * uc.setRequestProperty("X-Requested-With", "Curl");
     * uc.setRequestProperty("Authorization", "Basic
     * YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
     *
     * InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
     *
     * BufferedReader reader = new BufferedReader(reader2); StringBuilder out2 =
     * new StringBuilder(); String line; while ((line = reader.readLine()) !=
     * null) { out2.append(line); } System.out.println(out2.toString());
     * reader.close();
     *
     */
    // GET /customers/customer-token/charges
    /**
     * String stringUrl =
     * "https://test-api.pin.net.au/1/customers/cus_l1BAUZpQwsNiiSHRLFJrYA/charges";
     * URL url = new URL(stringUrl); URLConnection uc = url.openConnection();
     *
     * uc.setRequestProperty("X-Requested-With", "Curl");
     * uc.setRequestProperty("Authorization", "Basic
     * YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
     *
     * InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());
     *
     * BufferedReader reader = new BufferedReader(reader2); StringBuilder out2 =
     * new StringBuilder(); String line; while ((line = reader.readLine()) !=
     * null) { out2.append(line); } System.out.println(out2.toString());
     * reader.close();
     *
     */
    
    /**
    BufferedReader reader = new BufferedReader(reader2);
    StringBuilder out2 = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
        out2.append(line);
    }
    System.out.println(out2.toString());
    reader.close();
    **/
    
    // GET /customer
    /**
    String stringUrl = "https://test-api.pin.net.au/1/customers";
    URL url = new URL(stringUrl);
    URLConnection uc = url.openConnection();

    uc.setRequestProperty("X-Requested-With", "Curl");
    uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");

    InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());

    BufferedReader reader = new BufferedReader(reader2);
    StringBuilder out2 = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
        out2.append(line);
    }
    System.out.println(out2.toString());
    reader.close();
    **/
    
    // POST /charges
    /**
    String stringUrl = "https://test-api.pin.net.au/1/charges";
    URL url = new URL(stringUrl);
    HttpURLConnection uc = (HttpURLConnection)url.openConnection();

    uc.setDoOutput(true);
    uc.setInstanceFollowRedirects(false);
    uc.setRequestMethod("POST");
    uc.setRequestProperty("X-Requested-With", "Curl");
    uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");
    
    DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
    wr.writeBytes("email=test@test.com.au&"
            + "description=something&"
            + "amount=400&"
            + "ip_address=203.192.1.172&"
            + "customer_token=cus_l1BAUZpQwsNiiSHRLFJrYA"); //customer token here
    wr.flush();
    wr.close();

    InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());

    BufferedReader reader = new BufferedReader(reader2);
    StringBuilder out2 = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
        out2.append(line);
    }
    System.out.println(out2.toString());
    reader.close();
    **/
    
    // POST /customers
    /**
    String stringUrl = "https://test-api.pin.net.au/1/customers";
    URL url = new URL(stringUrl);
    HttpURLConnection uc = (HttpURLConnection) url.openConnection();

    uc.setDoOutput(true);
    uc.setInstanceFollowRedirects(false);
    uc.setRequestMethod("POST");
    uc.setRequestProperty("X-Requested-With", "Curl");
    uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");

    DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
    String stringhjk = "email=atestemail@test.com.au&"
            + "card[number]=5520000000000000&"
            + "card[expiry_month]=03&"
            + "card[expiry_year]=2017&"
            + "card[cvc]=123&"
            + "card[name]=Simon Test&"
            + "card[address_line1]=123 Something St&"
            + "card[address_line2]=&"
            + "card[address_city]=Sydney&"
            + "card[address_postcode]=2000&"
            + "card[address_state]=NSW&"
            + "card[address_country]=Australia";
    
    System.out.println(stringhjk);
    wr.writeBytes("email=atestemail@test.com.au&"
            + "card[number]=5520000000000000&"
            + "card[expiry_month]=03&"
            + "card[expiry_year]=2017&"
            + "card[cvc]=123&"
            + "card[name]=Simon Test&"
            + "card[address_line1]=123 Something St&"
            + "card[address_line2]=&"
            + "card[address_city]=Sydney&"
            + "card[address_postcode]=2000&"
            + "card[address_state]=NSW&"
            + "card[address_country]=Australia"); // card details here OR we can use card tokens...?
    wr.flush();
    wr.close();

    InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());

    BufferedReader reader = new BufferedReader(reader2);
    StringBuilder out2 = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
        out2.append(line);
    }
    
    JSONParser parser = new JSONParser();
    Object obj = parser.parse(out2.toString());

    JSONObject jsonObject = (JSONObject) obj;

    JSONObject responseJSON = (JSONObject) jsonObject.get("response");
    String token = (String)responseJSON.get("token");
    
    System.out.println(token);

    //String name = (String) jsonObject.get("token");
    //System.out.println(name);
    //System.out.println(out2.toString());
    uc.disconnect();
    reader.close();
    **/
    
    
    
    String stringUrl = "https://test-api.pin.net.au/1/customers/cus_yc_3OkLcZYSlCI1Rd6gMaA/charges";
URL url = new URL(stringUrl); URLConnection uc = url.openConnection();

uc.setRequestProperty("X-Requested-With", "Curl");
uc.setRequestProperty("Authorization", "Basic YnNhTkp6dHNqUm1RT19uOWlZd0RkUTo=");

InputStreamReader reader2 = new InputStreamReader(uc.getInputStream());

BufferedReader reader = new BufferedReader(reader2);
StringBuilder out2 = new StringBuilder();
String line;
while ((line = reader.readLine()) != null) {
    out2.append(line);
}

System.out.println(out2.toString());
reader.close();
    
%>
